const sliderContent = document.querySelector('.slider__content');
const slides = Array.from(sliderContent.children);
const buttonPrev = document.querySelector('#slider__button-prev');
const buttonNext = document.querySelector('#slider__button-next');

buttonNext.addEventListener('click', () => showSlide('next'));
buttonPrev.addEventListener('click', () => showSlide('pref'));

slides.forEach(tuneSlide);

function tuneSlide(slide, index) {
	if (index === 0) slide.classList.add('slider__slide_active');

	slide.dataset.index = index;
	slide.addEventListener('click', slideClickHandler);
}

function slideClickHandler() {
	console.log('test');
	showSlide();
}

function showSlide(direction = 'next') {
	const currentSlide = sliderContent.querySelector('.slider__slide_active');
	const currentIndex = +currentSlide.dataset.index;
	let nextIndex;

	if (direction === 'next') {
		nextIndex = currentIndex < slides.length - 1 ? currentIndex + 1 : 0;
		console.log('next');
	} else {
		nextIndex = currentIndex > 0 ? currentIndex - 1 : slides.length - 1;
	}

	slides[currentIndex].classList.remove('slider__slide_active');
	slides[nextIndex].classList.add('slider__slide_active');
}

setInterval(showSlide, 3000);
