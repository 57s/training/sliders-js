import { configDefault } from './configDefault.js';
class Slider {
	constructor(props) {
		this.initConfig(props);
		this.initSlider(props.slider);

		console.log(this);
	}

	initConfig = ({ slider, config }) => {
		this.config = { ...configDefault, ...config };

		this.dom = {};
		this.state = {};

		// DOM
		this.dom.slider = slider;
		this.dom.container = this.dom.slider.querySelector(
			this.config.classes.container
		);
		this.dom.track = this.dom.slider.querySelector(this.config.classes.track);
		this.dom.slides = this.dom.slider.querySelectorAll(
			this.config.classes.slide
		);
		this.dom.buttonPrev = this.dom.slider.querySelector(
			this.config.classes.buttonPrev
		);
		this.dom.buttonNext = this.dom.slider.querySelector(
			this.config.classes.buttonNext
		);

		// Constants
		this.state.index = 0;
		this.state.countSlides = this.dom.slides.length;
		this.state.containerWidth = this.dom.slider.offsetWidth;
		this.state.slideHeight = this.dom.slider.offsetHeight;
		this.state.slideWidth =
			this.dom.slider.offsetWidth / this.config.settings.slidesToShow;
		this.state.trackWidth = this.state.countSlides * this.state.slideWidth;
	};

	initSlider = slider => {
		this.dom.container.style.width = `${this.state.containerWidth}px`;
		this.dom.track.style.width = `${this.state.trackWidth}px`;
		this.dom.track.style.minHeight = `${this.state.slideHeight}px`;

		this.dom.slides.forEach(this.handlerSlide);

		this.dom.buttonPrev.addEventListener('click', this.goTrackPref);
		this.dom.buttonNext.addEventListener('click', this.goTrackNext);
	};

	handlerSlide = slide => {
		const width = this.state.slideWidth;
		slide.style.width = `${width}px`;
	};

	goTrackNext = () => {
		console.log('next');
		const nextIndex = this.state.index + this.config.settings.slidesToScroll;
		this.goNextSlide(nextIndex);
	};

	goTrackPref = () => {
		console.log('prev');
		const nextIndex = this.state.index - this.config.settings.slidesToScroll;
		this.goNextSlide(nextIndex);
	};

	moveTrack = () => {
		const offset = this.state.index * this.state.slideWidth;
		this.dom.track.style.transform = `translate(-${offset}px)`;
	};

	goNextSlide(newIndex) {
		const countSlides =
			this.state.countSlides - this.config.settings.slidesToScroll;

		if (newIndex < 0) {
			this.state.index = countSlides;
		} else if (newIndex > countSlides) {
			this.state.index = 0;
		} else {
			this.state.index = newIndex;
		}

		this.moveTrack();
	}
}

function initSliders(options) {
	console.log('init');
	const slidersList = document.querySelectorAll(configDefault.classes.slider);

	slidersList.forEach(slider => {
		let config;

		Object.entries(options).forEach(([className, conf]) => {
			if (slider.classList.contains(className)) {
				config = conf;
			}
		});

		new Slider({ slider, config });
	});
}

export { initSliders };
