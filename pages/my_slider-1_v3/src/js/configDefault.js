export const configDefault = {
	classes: {
		slider: '.slider',
		container: '.slider__container',
		track: '.slider__track',
		slide: '.slider__slide',
		buttonPrev: '.slider__button-prev',
		buttonNext: '.slider__button-next',
	},
	settings: {
		slidesToShow: 1,
		slidesToScroll: 1,
		gap: 0,
	},
};
