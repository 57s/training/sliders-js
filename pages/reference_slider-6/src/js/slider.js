const context = initContext();

function initContext() {
	const elementSlider = document.querySelector('.slider');

	const context = {
		index: 0,
		animationStatus: true,
		widthSlide: elementSlider.offsetWidth,
		widthHeight: elementSlider.offsetHeight,
		duration: 1000,
		images: [],
		slider: elementSlider,
		track: elementSlider.querySelector('.slider__track'),
		startSlides: elementSlider.querySelectorAll('.slider__slide'),
		activeSlides: elementSlider.getElementsByClassName('slider__slide'),
		btnPrev: elementSlider.querySelector('.slider__button-prev'),
		btnNext: elementSlider.querySelector('.slider__button-next'),
	};

	getSlides.call(context);

	return context;
}

function removeSlide(index) {
	const slide = context.activeSlides[index];
	slide.remove();
}

function createElementImages({ src, alt }) {
	const image = document.createElement('img');

	image.src = src;
	image.alt = alt;
	image.classList.add('slider__slide');
	image.style.transform = `translateX(-${context.offsetImg * context.index}px)`;
	image.style.width = `${context.widthSlide}px`;

	return image;
}

function drawImg(index = context.index, prepend = false) {
	const img = createElementImages(context.images[index]);

	if (prepend) {
		context.track.prepend(img);
	} else {
		context.track.append(img);
	}
}

function getSlides() {
	this.startSlides.forEach(slide => {
		this.images.push({
			src: slide.src,
			alt: slide.alt,
		});

		slide.remove();
	});
}

function incrementIndex() {
	let index = context.index;

	if (context.index < context.images.length - 1) {
		index = context.index + 1;
	} else {
		index = 0;
	}

	return index;
}

function decrementIndex() {
	let index = context.index;

	if (context.index > 0) {
		index = context.index - 1;
	} else {
		index = context.images.length - 1;
	}

	return index;
}

function animate({ duration, funcDraw, funcRemoveElem }) {
	const timeStart = performance.now();

	requestAnimationFrame(function animate(time) {
		let step = (time - timeStart) / duration;

		if (step > 1) step = 1;

		funcDraw(step);

		if (step < 1) {
			requestAnimationFrame(animate);
		} else {
			funcRemoveElem();
			context.animationStatus = true
		}
	});
}

function nextSlide() {
	if (!context.animationStatus) return;
	context.animationStatus = !context.animationStatus;

	context.index = incrementIndex();
	genNextSlide();
	animate({
		duration: context.duration,
		funcDraw: progress => {
			context.activeSlides[0].style.width = `${
				context.widthSlide * (1 - progress)
			}px`;
		},
		funcRemoveElem: () => removeSlide(0),
	});
}

function prevSlide() {
	if (!context.animationStatus) return;
	context.animationStatus = !context.animationStatus;

	context.index = decrementIndex();
	genPrevSlide();
	animate({
		duration: context.duration,
		funcDraw: progress => {
			context.activeSlides[0].style.width = `${
				context.widthSlide * progress
			}px`;
		},
		funcRemoveElem: () => removeSlide(context.activeSlides.length - 1),
	});
}

function genPrevSlide() {
	const prevSlide = decrementIndex();

	drawImg(prevSlide, true);
}

function genNextSlide() {
	const nextSlide = incrementIndex();

	drawImg(nextSlide);
}

function initSlider() {
	context.track.style.width = `${context.widthSlide * 3}px`;
	context.track.style.height = `${context.widthHeight}px`;
	context.track.style.transform = `translateX(-${context.widthSlide}px)`;

	context.btnNext.addEventListener('click', nextSlide);
	context.btnPrev.addEventListener('click', prevSlide);

	drawImg();
	genPrevSlide();
	genNextSlide();
}

export { initSlider };
