const CONFIG = {
	gap: 0,
	stepDetectSwipe: 50,
	animationDuration: 1_000,

	classes: {
		slider: 'js--slider',
		sliderDraggable: 'js--slider_draggable',
		container: 'js--slider__container',
		track: 'js--slider__track',
		slide: 'js--slider__slide',
	},
};

export { CONFIG };
