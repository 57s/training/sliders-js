import { debounce, wrapElement } from '../../../utils/utils.js';
import { SWIPE_DIRECTION } from './slider-constants.js';
import { CONFIG } from './slider-config.js';

class Slider {
	#_currentSlide;

	nodeContainer;
	nodeTrack;
	nodeCollectionSlides;

	startPointerPositionX;
	endPointerPositionX;

	isChangedSlide;
	goNextSlide;

	constructor({ element, options }) {
		this.nodeSlider = element;
		this.nodeInboxItems = Array.from(element.children);
		this.countSlides = element.childElementCount;

		this.#_currentSlide = 0;
		this.gap = options?.gap ?? CONFIG.gap;
		this.init();
	}

	get slideWidth() {
		return this.nodeSlider.getBoundingClientRect().width;
	}

	get trackWidth() {
		return this.slideWidth * this.countSlides;
	}

	get trackShift() {
		const isOut =
			(this.index === 0 && this.shiftPointerPositionX < 0) ||
			(this.index === this.countSlides - 1 && this.shiftPointerPositionX > 0);

		const offsetSlides = (this.slideWidth + this.gap) * this.index;
		const easing = this.shiftPointerPositionX / 5;
		const offsetPointer = isOut ? easing : this.shiftPointerPositionX;
		const offsetTrack = offsetSlides + offsetPointer;

		const shift = offsetTrack * -1;

		return shift;
	}

	get shiftPointerPositionX() {
		if (this.endPointerPositionX && this.startPointerPositionX) {
			return this.startPointerPositionX - this.endPointerPositionX;
		} else {
			return 0;
		}
	}

	get index() {
		return this.#_currentSlide;
	}

	set index(newIndex) {
		if (!(newIndex >= this.countSlides || newIndex < 0)) {
			this.#_currentSlide = newIndex;
		}
	}

	init = () => {
		this.handlerSlider();

		this.nodeContainer = this.createContainer();
		this.nodeTrack = this.createTrack();
		this.nodeCollectionSlides = this.handlerCollectionSlides();

		this.nodeTrack.appendChild(this.nodeCollectionSlides);
		this.nodeContainer.appendChild(this.nodeTrack);
		this.nodeSlider.appendChild(this.nodeContainer);

		this.setParameters();
		this.setEvents();
		this.setStyleTrack();
	};

	setEvents = () => {
		window.addEventListener('resize', debounce(this.setParameters));
		this.nodeSlider.addEventListener('pointerdown', this.dragStart);
	};

	setParameters = () => {
		this.setStyleTrack();
		this.handlerTrack();

		Array.from(this.nodeTrack.children).forEach((slide) => {
			slide.style.width = `${this.slideWidth}px`;
		});
	};

	setStyleTrack = ({ x = this.trackShift, transition = false } = {}) => {
		const duration = transition ? CONFIG.animationDuration : 0;
		this.nodeTrack.style.transition = `${duration}ms`;
		this.nodeTrack.style.transform = `translateX(${x}px)`;
	};

	dragStart = (pointerEvent) => {
		this.isChangedSlide = false;
		this.setStyleTrack({ transition: true });
		this.startPointerPositionX = pointerEvent.pageX;
		this.nodeSlider.classList.add(CONFIG.classes.sliderDraggable);
		this.nodeSlider.addEventListener('pointermove', this.dragMove);
		document.addEventListener('pointerup', this.dragEnd);
	};

	dragMove = (pointerEvent) => {
		this.endPointerPositionX = pointerEvent.pageX;
		this.setStyleTrack();

		if (!this.isChangedSlide) {
			const swipe = this.detectSwipe();

			if (swipe) {
				this.isChangedSlide = true;
				this.goNextSlide = swipe;
			}
		}
	};

	dragEnd = (pointerEvent) => {
		if (this.goNextSlide === SWIPE_DIRECTION.left) {
			this.nextSlide();
		} else if (this.goNextSlide === SWIPE_DIRECTION.right) {
			this.prevSlide();
		}

		this.isChangedSlide = true;
		this.startPointerPositionX = null;
		this.endPointerPositionX = null;
		this.nodeSlider.classList.remove(CONFIG.classes.sliderDraggable);

		this.nodeSlider.removeEventListener('pointermove', this.dragMove);
		document.removeEventListener('pointerup', this.dragEnd);
		this.setStyleTrack({
			transition: true,
		});
	};

	detectSwipe = () => {
		if (Math.abs(this.shiftPointerPositionX) < CONFIG.stepDetectSwipe) {
			return false;
		}

		if (this.shiftPointerPositionX > 0) {
			return SWIPE_DIRECTION.left;
		} else {
			return SWIPE_DIRECTION.right;
		}
	};

	nextSlide = () => {
		this.index++;
	};

	prevSlide = () => {
		this.index--;
	};

	createContainer = () => {
		const container = document.createElement('div');
		container.classList.add(CONFIG.classes.container);

		return container;
	};

	createTrack = () => {
		const track = document.createElement('div');
		track.classList.add(CONFIG.classes.track);

		return track;
	};

	createSlide = () => {
		const slide = document.createElement('div');
		slide.classList.add(CONFIG.classes.slide);

		return slide;
	};

	handlerCollectionSlides = () => {
		const slides = document.createDocumentFragment();

		this.nodeInboxItems.forEach((item) => {
			const wrapper = this.createSlide();
			const slide = wrapElement(item, wrapper);

			slides.appendChild(slide);
		});

		return slides;
	};

	handlerSlider = () => {
		this.nodeSlider.classList.add(CONFIG.classes.slider);
		this.nodeSlider.innerHTML = '';
	};

	handlerTrack = () => {
		this.nodeTrack.style.width = `${this.trackWidth}px`;
		this.nodeTrack.style.gap = `${this.gap}px`;
	};
}

export { Slider };
