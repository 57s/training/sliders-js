const SWIPE_DIRECTION = {
	left: 'LEFT',
	right: 'RIGHT',
};

export { SWIPE_DIRECTION };
