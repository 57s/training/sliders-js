function wrapElement(element, wrapper) {
	wrapper.appendChild(element);
	return wrapper;
}

function debounce(callback, delay = 100) {
	let timer;

	return function (...params) {
		clearTimeout(timer);
		timer = setTimeout(callback, delay, ...params);
	};
}

export { wrapElement, debounce };
