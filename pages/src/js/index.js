const elementSlider = document.querySelector('.slider');
const elementContainer = document.querySelector('.slider__container');
const elementTrack = elementSlider.querySelector('.slider__track');
const elementsSlides = elementTrack.querySelectorAll('.slider__slide');
const buttonPref = elementSlider.querySelector('#slider__button-prev');
const buttonNext = elementSlider.querySelector('#slider__button-next');

let index = 0;
let offset = 0;

function offsetTrack() {
	console.log(offset);
	elementTrack.style.transform = `translate(-${offset}px)`;
}

function goTrackPref() {
	console.log('PREF');

	if (offset <= 0) {
		offset = 1000;
	} else {
		offset -= 500;
	}

	offsetTrack();
}

function goTrackNext() {
	console.log('NEXT');

	if (offset >= 1000) {
		offset = 0;
	} else {
		offset += 500;
	}

	offsetTrack();
}
function initSlider() {
	const countSlides = elementsSlides.length

	console.log(countSlides);
}

initSlider()

buttonPref.addEventListener('click', goTrackPref);
buttonNext.addEventListener('click', goTrackNext);
