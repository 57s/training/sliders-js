const sliderLine = document.querySelector('.slider__line');
const sliderButtonPrev = document.querySelector('#slider__button-prev');
const sliderButtonNext = document.querySelector('#slider__button-next');

sliderButtonNext.addEventListener('click', next);
sliderButtonPrev.addEventListener('click', pref);

let offset = 0;

function next() {
	offset += 500;
	if (offset > 2000) offset = 0;
	sliderLine.style.left = -offset + 'px';
}

function pref() {
	offset -= 500;
	if (offset < 0) offset = 2000;
	sliderLine.style.left = -offset + 'px';
}

setInterval(next, 3000);
