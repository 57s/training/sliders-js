function initSliders() {
	const elementSlider = document.querySelector('.slider');
	const elementTrack = elementSlider.querySelector('.slider__track');
	const elementsSlides = elementSlider.querySelectorAll('.slider__slide');
	const buttonNext = elementSlider.querySelector('.slider__button-next');

	const arraySlides = Array.from(elementsSlides);
	const slidesWidth = [0].concat(arraySlides.map(slide => slide.offsetWidth));
	const mapSlidesOffset = slidesWidth.reduce(
		(prev, current) => {
			prev.acc += current;
			prev.array.push(prev.acc);
			return prev;
		},
		{ acc: 0, array: [] }
	).array;

	const countSlides = mapSlidesOffset.length - 1;
	const trackWidth = mapSlidesOffset[countSlides];
	const sliderWidth = elementSlider.offsetWidth;

	let index = 0;

	elementTrack.style.width = `${trackWidth}px`;

	const calculateOffset = () => {
		const currentOffset = mapSlidesOffset[index];
		const restOffset = trackWidth - currentOffset;

		if (restOffset > sliderWidth) {
			return currentOffset;
		} else {
			index = -1;
			return trackWidth - sliderWidth;
		}
	};

	function offsetTrack() {
		const offset = calculateOffset();
		elementTrack.style.transform = `translateX(-${offset}px)`;
	}

	function trackNext() {
		if (index >= countSlides - 1) {
			index = 0;
		} else {
			index++;
		}

		offsetTrack();
	}

	buttonNext.addEventListener('click', trackNext);
}

export { initSliders };
