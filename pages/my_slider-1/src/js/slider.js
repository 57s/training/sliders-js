const configDefault = {
	classes: {
		slider: '.slider',
		container: '.slider__container',
		track: '.slider__track',
		slide: '.slider__slide',
		buttonPrev: '.slider__button-prev',
		buttonNext: '.slider__button-next',
	},
	settings: {},
};

//  dom state

function initContext(slider, config = {}) {
	config = { ...configDefault, ...config };

	const dom = {
		container: slider.querySelector(config.classes.container),
		track: slider.querySelector(config.classes.track),
		slides: slider.querySelectorAll(config.classes.slide),
		buttonPref: slider.querySelector(config.classes.buttonPrev),
		buttonNext: slider.querySelector(config.classes.buttonNext),
	};

	const constants = {
		countSlides: dom.slides.length,
		slideWidth: slider.offsetWidth,
		slideHeight: slider.offsetHeight,
	};

	const calculate = {
		trackWidth: constants.countSlides * constants.slideWidth,
	};

	const state = {
		index: 0,
		offsetTrack: 0,
	};

	return {
		config,
		dom,
		constants,
		calculate,
		state,
	};
}

function offsetTrack() {
	this.state.offset = this.state.index * this.constants.slideWidth;
	this.dom.track.style.transform = `translate(-${this.state.offset}px)`;
}

function goTrackPref() {
	if (this.state.index > 0) {
		this.state.index -= 1;
	} else {
		this.state.index = this.constants.countSlides - 1;
	}

	offsetTrack.call(this);
}

function goTrackNext() {
	if (this.state.index < this.constants.countSlides - 1) {
		this.state.index += 1;
	} else {
		this.state.index = 0;
	}

	offsetTrack.call(this);
}

function handlerSlider(slider) {
	const context = initContext(slider);
	console.log(context);

	context.dom.container.style.width = `${context.constants.slideWidth}px`;
	context.dom.track.style.width = `${context.calculate.trackWidth}px`;
	context.dom.track.style.minHeight = `${context.constants.slideHeight}px`;
	context.dom.slides.forEach(handlerSlide);

	function handlerSlide(slide) {
		slide.style.width = `${context.constants.slideWidth}px`;
	}

	context.dom.buttonPref.addEventListener('click', goTrackPref.bind(context));
	context.dom.buttonNext.addEventListener('click', goTrackNext.bind(context));
}

function initSliders() {
	const slidersList = document.querySelectorAll(configDefault.classes.slider);

	slidersList.forEach(handlerSlider);
}

export { initSliders };
