const dom = {
	track: document.querySelector('.slider__track'),
	container: document.querySelector('.slider__container'),
	slides: document.querySelectorAll('.slider__slide'),
	buttonPrev: document.querySelector('#slider__button-prev'),
	buttonNext: document.querySelector('#slider__button-next'),
};

const conf = {
	slidesToShow: 3,
	slidesToScroll: 3,
};

dom.buttonNext.addEventListener('click', next);
dom.buttonPrev.addEventListener('click', pref);

let position = 0;

// Количество слайдов
const slidesCount = dom.slides.length;
// Ширина слайда
const slideWidth = dom.container.offsetWidth / conf.slidesToShow;
// Позиция на которую нужно сдвигать `track`
const movePosition = conf.slidesToScroll * slideWidth;

// Перебираем слайды
dom.slides.forEach(tuneSlide);

function tuneSlide(slide, index) {
	slide.style.width = slideWidth + 'px';
	// slide.addEventListener('click', slideClickHandler);
}

function slideClickHandler() {}

function moveTrack(pos) {
	dom.track.style.transform = `translateX(${pos}px)`;
}

function next() {
	// Количество проскроленный пикселей
	const scroll = Math.abs(position) + conf.slidesToShow * slideWidth;
	// Количество проскроленный слайдов
	const scrollSlides = scroll / slideWidth;
	// остаток слайдов
	const slidesLift = slidesCount - scrollSlides;
	// количество пикселей остывших слайдов
	const scrollSlidesLift = slidesLift * slideWidth;

	// Если остаток слайдов меньше чем количество для скролла,
	// то проскролить на остаток
	position -=
		slidesLift >= conf.slidesToScroll ? movePosition : scrollSlidesLift;
	moveTrack(position);
	checkButton();
}

function pref() {
	const slidesLift = Math.abs(position) / slideWidth;
	const scrollSlidesLift = slidesLift * slideWidth;

	position +=
		slidesLift >= conf.slidesToScroll ? movePosition : scrollSlidesLift;
	moveTrack(position);
	checkButton();
}

function checkButton() {
	dom.buttonNext.disabled =
		// Если позиция трека меньше или равна суммарному остатку слайдов
		// то сделать кнопку не активной
		position <= -(slidesCount - conf.slidesToShow) * slideWidth;
	dom.buttonPrev.disabled = position === 0;
}

checkButton();
