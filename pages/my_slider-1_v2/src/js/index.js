import { initSliders } from './slider.js';

const config = {
	['slider_several']: {
		settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			gap: 30
		},
	},
};

initSliders(config);
