const configDefault = {
	classes: {
		slider: '.slider',
		container: '.slider__container',
		track: '.slider__track',
		slide: '.slider__slide',
		buttonPrev: '.slider__button-prev',
		buttonNext: '.slider__button-next',
	},
	settings: {
		slidesToShow: 1,
		slidesToScroll: 1,
		gap: 0,
	},
};

//  dom state

function initContext(slider, config = {}) {
	config = { ...configDefault, ...config };

	const dom = {
		slider,
		container: slider.querySelector(config.classes.container),
		track: slider.querySelector(config.classes.track),
		slides: slider.querySelectorAll(config.classes.slide),
		buttonPref: slider.querySelector(config.classes.buttonPrev),
		buttonNext: slider.querySelector(config.classes.buttonNext),
	};

	//
	console.log(dom.track.style.gap);
	config.settings.gap = 0;

	// Constants
	const countSlides = dom.slides.length;
	const containerWidth = dom.slider.offsetWidth;
	const slideHeight = dom.slider.offsetHeight;

	// Calculate
	const slideWidth = dom.slider.offsetWidth / config.settings.slidesToShow;
	const trackWidth = countSlides * slideWidth;

	const state = {
		slideWidth,
		countSlides,
		containerWidth,
		slideHeight,
		trackWidth,

		index: 0,
		offsetTrack: 0,
	};

	return {
		config,
		dom,
		state,
	};
}

function offsetTrack() {
	this.state.offset = this.state.index * this.state.slideWidth;
	this.dom.track.style.transform = `translate(-${this.state.offset}px)`;
}

function fixIndex() {
	const countSlides =
		this.state.countSlides - this.config.settings.slidesToScroll;

	if (this.state.index < 0) {
		this.state.index = countSlides;
	}

	if (this.state.index > countSlides) {
		this.state.index = 0;
	}
}

function goTrackPref() {
	this.state.index -= this.config.settings.slidesToScroll;

	fixIndex.call(this);
	offsetTrack.call(this);
}

function goTrackNext() {
	this.state.index += this.config.settings.slidesToScroll;

	fixIndex.call(this);
	offsetTrack.call(this);
}

function handlerSlider(slider, config) {
	const context = initContext(slider, config);

	context.dom.container.style.width = `${context.state.containerWidth}px`;
	context.dom.track.style.width = `${context.state.trackWidth}px`;
	context.dom.track.style.minHeight = `${context.state.slideHeight}px`;
	context.dom.slides.forEach(handlerSlide);

	function handlerSlide(slide) {
		const width = context.state.slideWidth;
		slide.style.width = `${width}px`;
	}

	context.dom.buttonPref.addEventListener('click', goTrackPref.bind(context));
	context.dom.buttonNext.addEventListener('click', goTrackNext.bind(context));
}

function initSliders(options) {
	const slidersList = document.querySelectorAll(configDefault.classes.slider);

	slidersList.forEach(slide => {
		let config;

		// Найти переданный конфиг
		Object.entries(options).forEach(([className, conf]) => {
			if (slide.classList.contains(className)) {
				config = conf;
			}
		});

		handlerSlider(slide, config);
	});
}

export { initSliders };
