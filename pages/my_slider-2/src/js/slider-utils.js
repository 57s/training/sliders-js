const initConfig = {
	classes: {
		slider: 'slider',
		container: 'slider__container',
		track: 'slider__track',
		slide: 'slider__slide',
		image: 'slider__image',
		control: 'slider__control',
		btnPrev: 'slider__btn-prev',
		btnNext: 'slider__btn-next',
	},
};

const globalStyles = {
	height: '100%',
	width: '100%',
	overflow: 'hidden',
};

function addStyle(element, styles) {
	for (const key in styles) {
		element.style[key] = styles[key];
	}
}
function createSlide({ src, alt, id }) {
	const slide = document.createElement('div');
	const image = document.createElement('img');

	image.src = src;
	image.alt = alt;
	image.classList.add(initConfig.classes.image);
	addStyle(image, globalStyles);

	slide.classList.add(initConfig.classes.slide, `${initConfig.classes.slide}_${id}`);
	slide.appendChild(image);
	addStyle(slide, globalStyles);

	return slide;
}

function createControl() {
	const control = document.createElement('div');
	const btnNext = document.createElement('button');
	const btnPrev = document.createElement('button');

	btnNext.innerText = 'Next';
	btnNext.classList.add(initConfig.classes.btnNext);
	btnPrev.innerText = 'Prev';
	btnPrev.classList.add(initConfig.classes.btnPrev);

	control.append(btnNext, btnPrev);
	control.classList.add(initConfig.classes.control);

	return control;
}

function createSlider({ images, settings }) {
	const slides = images.map(createSlide);
	const control = createControl();
	const slider = document.createElement('div');
	const container = document.createElement('div');
	const track = document.createElement('div');

	slider.appendChild(container);
	slider.appendChild(track);
	slider.appendChild(control);
	slider.classList.add(initConfig.classes.slider);
	slider.style.width = `${settings.width}px`;
	slider.style.height = `${settings.height}px`;

	container.appendChild(track);
	container.classList.add(initConfig.classes.container);
	container.style.boxShadow = `0 0 15px black`;
	addStyle(container, globalStyles);

	track.append(...slides);
	track.classList.add(initConfig.classes.track);
	track.style.width = `${settings.width * images.length}px`;
	track.style.height = `${settings.height}px`;
	track.style.display = 'grid';
	track.style.gridAutoFlow = 'column';
	track.style.transition = `${settings.duration}s`;

	return slider;
}

export { createSlider, initConfig };