import { createSlider, initConfig } from './slider-utils.js';

let state;

function initSlider(conf) {
	state = conf;

	const slider = createSlider(state);
	const btnNext = slider.querySelector(`.${initConfig.classes.btnNext}`);
	const btnPrev = slider.querySelector(`.${initConfig.classes.btnPrev}`);

	state.track = slider.querySelector(`.${initConfig.classes.track}`);
	state.index = conf.images.find(img => img.start).index;

	btnNext.addEventListener('click', nextSlide.bind(null, 'next'));
	btnPrev.addEventListener('click', nextSlide.bind(null, 'prev'));
	conf.root.append(slider);
	offsetSlide();
}

function offsetSlide() {
	const offset = state.settings.width * state.index;
	state.track.style.transform = `translateX(-${offset}px)`;
}

function nextSlide(direction = 'next') {
	if (direction === 'prev') {
		state.index++;
		if (state.index >= state.images.length) {
			state.index = 0;
		}
	} else if (direction === 'next') {
		state.index--;
		if (state.index < 0) {
			state.index = state.images.length - 1;
		}
	}

	offsetSlide();
}
export { initSlider };
