import { IMAGES_SLIDES } from "./data.js";
import { initSlider } from "./slider.js";

const elementMain = document.querySelector('.main');

const config = {
	root: elementMain,
	images: IMAGES_SLIDES,
	settings: {
		width: 700,
		height: 500,
		duration: 1,
	},
};

initSlider(config);
