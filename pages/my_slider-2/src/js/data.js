const IMAGES_SLIDES = [
	{ index: 0, src: './src/img/1.jpg', alt: 'Slide 1' },
	{ index: 1, src: './src/img/2.jpg', alt: 'Slide 2' },
	{ index: 2, src: './src/img/3.jpg', alt: 'Slide 3' },
	{ index: 3, src: './src/img/4.jpg', alt: 'Slide 4', start: true },
	{ index: 4, src: './src/img/5.jpg', alt: 'Slide 5' },
	{ index: 5, src: './src/img/6.jpg', alt: 'Slide 6' },
	{ index: 6, src: './src/img/7.jpg', alt: 'Slide 7' },
	{ index: 7, src: './src/img/8.jpg', alt: 'Slide 8' },
	{ index: 8, src: './src/img/9.jpg', alt: 'Slide 9' },
];

export { IMAGES_SLIDES };
