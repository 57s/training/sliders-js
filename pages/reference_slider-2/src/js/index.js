const slider = document.querySelector('.slider');
const carousel = document.querySelector('.slider__carousel');
const slides = document.querySelectorAll('.slider__slide');
const buttonPrev = document.querySelector('#slider__button-prev');
const buttonNext = document.querySelector('#slider__button-next');

window.addEventListener('resize', init);
buttonNext.addEventListener('click', next);
buttonPrev.addEventListener('click', pref);

let offset = 0;
let index = 0;
let sliderWidth;

function init() {
	sliderWidth = slider.offsetWidth;
	carousel.style.width = sliderWidth * slides.length + 'px';

	slides.forEach(slide => {
		slide.style.width = sliderWidth + 'px';
		slide.style.height = 'auto';
	});

	drawSlide(); // Фикс слайда при изменение ширины окна
}

function next() {
	index++;
	if (index >= slides.length) index = 0;
	drawSlide();
}

function pref() {
	index--;
	if (index < 0) index = slides.length - 1;
	drawSlide();
}

function drawSlide() {
	carousel.style.transform = 'translate(-' + index * sliderWidth + 'px)';
}

setInterval(next, 3000);

init();
