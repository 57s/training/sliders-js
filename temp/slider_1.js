const elementSlider = document.querySelector('.slider');
const elementTrack = elementSlider.querySelector('.slider__track');
const buttonPref = elementSlider.querySelector('#slider__button-prev');
const buttonNext = elementSlider.querySelector('#slider__button-next');

let offset = 0;

function offsetTrack() {
	console.log(offset);
	elementTrack.style.transform = `translate(-${offset}px)`;
}

function goTrackPref() {
	console.log('PREF');

	if (offset <= 0) {
		offset = 1000;
	} else {
		offset -= 500;
	}

	offsetTrack();
}

function goTrackNext() {
	console.log('NEXT');

	if (offset >= 1000) {
		offset = 0;
	} else {
		offset += 500;
	}

	offsetTrack();
}

buttonPref.addEventListener('click', goTrackPref);
buttonNext.addEventListener('click', goTrackNext);
