function initSliders() {
	const elementSlider = document.querySelector('.slider');
	const elementTrack = elementSlider.querySelector('.slider__track');
	const elementsSlides = elementSlider.querySelectorAll('.slider__slide');
	const buttonNext = elementSlider.querySelector('.slider__button-next');

	const countSlides = elementsSlides.length;
	const slideWidth = elementSlider.offsetWidth;
	const trackWidth = slideWidth * countSlides;

	let index = 0;
	elementTrack.style.width = `${trackWidth}px`;

	function offsetTrack() {
		const offset = slideWidth * index;
		elementTrack.style.transform = `translateX(-${offset}px)`;
	}

	function trackNext() {
		index++;

		if (index > countSlides - 1) {
			elementsSlides.push(elementsSlides.shift());
		}

		offsetTrack();
	}

	buttonNext.addEventListener('click', trackNext);
}

export { initSliders };
